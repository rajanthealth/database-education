# README #


### What is this repository for? ###

* This repo is for Alina, Alison, and Evangelie to educate ourselves on databases and distributed computing, as we start setting up our work environments for data analysis.
* Last updated December 2021

### How do I get set up? ###

All to be added. 

* Needed docker containers
* Conda environment
* Dependencies
* Database configuration
* Deployment instructions


### Who do I talk to? ###

* Contact Evangelie (ezachos@rajant) with any questions